import * as  dotenv from 'dotenv'
dotenv.config({ path: '.env' })
dotenv.config({ path: `.env.local` })

let env = process.env.EGG_SERVER_ENV;
dotenv.config({ path: `.env.${env}` })
dotenv.config({ path: `.env.${env}.local` })
