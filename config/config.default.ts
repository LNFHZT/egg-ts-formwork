import { EggAppConfig, EggAppInfo, PowerPartial } from 'egg';
import './loadEnv';
export default (appInfo: EggAppInfo) => {
  const config = {} as PowerPartial<EggAppConfig>;

  // override config from framework / plugin
  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1691658265504_3490';

  // add your egg config in here
  config.middleware = ['json'];
  // add your special config in here
  const bizConfig = {
    // sourceUrl: `https://github.com/eggjs/examples/tree/master/${appInfo.name}`,
  };

  // the return config will combines to EggAppConfig 

  // 数据库配置
  let { VIS_DATABASE_dialect } = process.env;
  switch (VIS_DATABASE_dialect) {
    case 'mysql':
      config.sequelize = {
        dialect: process.env.VIS_MYSQL_dialect,
        host: process.env.VIS_MYSQL_host,
        port: process.env.VIS_MYSQL_port,
        database: process.env.VIS_MYSQL_database,
        username: process.env.VIS_MYSQL_username,
        password: process.env.VIS_MYSQL_password,
        define: {
          // 取消数据表名复数
          freezeTableName: true,
          // 自动写入时间戳 created_at updated_at
          timestamps: true,
          createdAt: 'created_at',
          updatedAt: 'updated_at',
          // 所有驼峰命名格式化
          underscored: true
        }
      };
      break;
    case 'sqlite':
      config.sequelize = {
        dialect: process.env.VIS_MYSQL_dialect,
        storage: process.env.VIS_SQLITE_storage
      };
      break;
    default:
      break;
  }
  // config.redis = {
  //   client: {
  //     port: 6379,          // 端口
  //     host: '127.0.0.1',   // host
  //     password: 'root',
  //     db: 0,
  //   },
  // };

  return {
    ...config,
    ...bizConfig,
  };
};
