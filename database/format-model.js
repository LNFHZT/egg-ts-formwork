const fs = require("fs");
const path = require("path");
function findSync(startpaths) {
  let result = [];

  function finder(paths) {
    let files = fs.readdirSync(paths);
    files.forEach((val, index) => {
      let fpaths = path.join(paths, val);

      let stats = fs.statSync(fpaths);

      if (stats.isDirectory()) finder(fpaths);

      if (stats.isFile())
        result.push({
          path: fpaths,
          name: val.replace(".ts", ""),
        });
    });
  }

  finder(startpaths);

  return result;
}
// 手动格式化 model
let fileNames = findSync("./app/model");
fileNames.forEach(item => {
  let content = fs.readFileSync(item.path, "utf8");
  if (!content.includes(`import { Application } from 'egg';`)) {
    content = `import { Application } from 'egg';\n${content}`;
  }
  // console.log(item.name);
  if (
    !content.includes(
      `\n\nexport default (app: any):any => {\n return class ${capitalize(item.name)} extends ${
        item.name
      }.initModel(app.model) { }\n}`
    )
  ) {
    content += `\n\nexport default (app: any):any => {\n return class  ${capitalize(
      item.name
    )} extends ${item.name}.initModel(app.model) { }\n}`;
  }
  fs.writeFileSync(item.path, content, "utf8");
});

function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
// export default (app: Application) => {
//   return class extends user.initModel(app.model) { }
// }
