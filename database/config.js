let { VIS_DATABASE_dialect } = process.env,
  data = {};
switch (VIS_DATABASE_dialect) {
  case "mysql":
    data = {
      dialect: process.env.VIS_MYSQL_dialect,
      host: process.env.VIS_MYSQL_host,
      port: process.env.VIS_MYSQL_port,
      database: process.env.VIS_MYSQL_database,
      username: process.env.VIS_MYSQL_username,
      password: process.env.VIS_MYSQL_password,
    };
    break;
  case "sqlite":
    data = {
      dialect: process.env.VIS_MYSQL_dialect,
      storage: process.env.VIS_SQLITE_storage,
    };
    break;
  default:
    break;
}
module.exports = {
  [process.env.VIS_SEQUELIZERC_ENV]: data,
};
