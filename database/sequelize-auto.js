const sequelizerc = require("../.sequelizerc");
const SequelizeAuto = require("sequelize-auto");
const config = require("./config.js");

//各个数据库配置
let dbConfig = [config[sequelizerc.env]];

let auto;
for (const data of dbConfig) {
  auto = new SequelizeAuto(data.database, data.username, data.password, {
    host: data.host,
    dialect: data.dialect,
    port: data.port,
    caseModel: "c", // convert snake_case column names to camelCase field names: user_id -> userId
    caseFile: "c", // file names created for each model use camelCase.js not snake_case.js
    caseProp: "c",
    singularize: true, // convert plural table names to singular model names
    lang: "ts",
    noInitModels: true,
    noAlias: true,
    useDefine: true,
    directory: "./app/model",
    additional: {
      // freezeTableName: true,
      //启动默认时间戳
      timestamps: true,
      // 自定义创建时间和修改时间字段
      updatedAt: "gmt_modified",
      createdAt: "gmt_create",
    },
  });
}
auto.run(function (err) {
  if (err) throw err;
  console.log(auto.tables); // table list
  console.log(auto.foreignKeys); // foreign key list
});
