# 架构说明

## 目录结构

```
testxx
├─ .cz-config.js git cz 配置文件
├─ .env 环境变量配置
├─ .env.development 环境变量配置 需要自己创建
├─ .env.local 环境变量配置 需要自己创建
├─ .env.production 环境变量配置 需要自己创建
├─ .env.test 环境变量配置 需要自己创建
├─ .eslintignore
├─ .eslintrc
├─ .gitignore
├─ .sequelizerc sequelizerc-cli 配置
├─ app
│  ├─ controller
│  ├─ public
│  └─ service
├─ config 配置模块
│  ├─ config.default.ts
│  ├─ loadEnv.ts
│  └─ plugin.ts
├─ database 数据库相关
├─ package.json
├─ pnpm-lock.yaml
├─ README.md
├─ tsconfig.json
└─ typings

```

## 推荐环境

本地调试时候采用的环境

node v18.12.1  
pnpm 7.18.2

## 安装教程

1.进入项目目录执行命令，下载依赖  
推荐使用 pnpm 进行安装依赖,已验证使用 pnpm 安装（以上环境）依赖暂无问题

```
pnpm i
```

2.创建配置文件.env.local  
因为是数据库相关的配置文件，所以不会提交到 git 上，需要用户手动添加

```
# 此配置为可选只有.env.local文件下新增 其他环境下不新增此配置
# .sequelizerc-cli 的环境变量
VIS_SEQUELIZERC_ENV=development
VIS_SEQUELIZERC_DEBUG=false

#以下数据库配置二选一
# 使用mysql数据库修改.env文件的VIS_DATABASE_dialect值（默认值mysql），修改为mysql
# 使用sqlite数据库修改.env文件的VIS_DATABASE_dialect值（默认值mysql），修改为sqlite

# mysql数据库配置
#账号
VIS_MYSQL_username=账号
#密码
VIS_MYSQL_password=密码
#数据库名
VIS_MYSQL_database=数据库名
#数据库地址
VIS_MYSQL_host=数据库地址
#链接数据库类型
VIS_MYSQL_dialect=mysql

#  sqlite数据库配置
#链接数据库类型
VIS_SQLITE_storage=sqlite
#数据库地址
VIS_SQLITE_dialect=database/draw_game.sqlite
```

不同环境下，连接数据库不同.env.development , .env.production ,.env.test 同上配置

3.启动项目

```
<!-- 启动本地环境 local -->
npm run dev
<!-- 启动开发环境 development -->
npm run dev:dev
<!-- 启动测试环境 test -->
npm run dev:test
<!-- 启动线上环境 production -->
npm run dev:prod
```

### 环境变量配置

项目采用使用 dotenv 从你的 环境目录 中的下列文件加载额外的环境变量：

```
.env                # 所有情况下都会加载
.env.local          # 所有情况下都会加载，但会被 git 忽略
.env.[mode]         # 只在指定模式下加载，但会被 git 忽略
.env.[mode].local   # 只在指定模式下加载，但会被 git 忽略

```

#### 环境加载优先级

一份用于指定模式（cross-env EGG_SERVER_ENV=指定环境）的文件（例如 .env.production）会比通用形式的优先级更高（例如 .env）。

另外，项目执行时已经存在的环境变量有最高的优先级，不会被 .env 类文件覆盖。

配置文件加载顺序

1. .env
2. .env.local
3. .env.[mode]
4. .env.[mode].local

#### 指定环境

```
<!-- 设置 EGG_SERVER_ENV 启动指定环境 -->
cross-env EGG_SERVER_ENV=test egg-bin dev
```

## 项目功能说明

### egg 装饰器详解

参考文档
https://blog.csdn.net/m0_60846226/article/details/129718834?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168535608116800222862165%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=168535608116800222862165&biz_id=&utm_medium=distribute.pc_search_result.none-task-code-2~all~first_rank_ecpm_v1~rank_v31_ecpm-1-129718834-26-null-null.142%5Ev88%5Econtrol,239%5Ev2%5Einsert_chatgpt&utm_term=%40eggjs%2Ftegg

### json 中间件详解

\app\middleware\json.ts  
controller 中 return 返回的数据将会 在 json 中间件处理格式化返回结果

```
 {
  code:200,
  data:{},
  msg:'ok',
  }
```

如果想要接口抛出错误，则需要在指定代码位置加入如下代码，则经过 json 格式化返回指定

```
<!-- 情况1 -->
throw new Error("");
<!-- 接口返回 -->
 {
  code:500,
  data:{},
  msg:'系统有误',
  }
 <!-- 情况2 -->
throw new Error("弹出一个错误");
<!-- 接口返回 -->
 {
  code:500,
  data:{},
  msg:'弹出一个错误',
  }
   <!-- 情况3 -->
throw new Error("弹出2个错误|90016");
<!-- 接口返回 -->
 {
  code:90016,
  data:{},
  msg:'弹出2个错误',
  }
```

### sequelizerc-cli

数据库环境配置 .env 或 .env.local 文件下的 VIS_SEQUELIZERC_ENV 字段修改环境配置
修改不同环境，读取不同的数据库配置
（参考文档 https://www.jianshu.com/p/14a34a310b84 ）

#### 数据库相关操作

使用 sequelizerc-cli 创建数据库相关表是为了更好的留下数据库表的更新记录

1. 创建数据库

```
<!-- 创建数据库  -->
npm run create
```

2. 删除数据库（<span style="font-weight: bold;color:red;">除了本地数据库可使用外不推荐其他环境下使用以下命令</span>）

```
<!-- 删除数据库 慎用  -->
npm run drop
```

3. 创建迁移记录 创建/删除/更新数据库表或数据库表字段

```
<!-- 创建迁移记录自动创建js  目录 database\migrations -->
npm run add [文件名]

<!-- 方法说明 -->
<!-- 创建表 -->
queryInterface.createTable('xxxx',{})
<!-- 删除表 -->
queryInterface.dropTable('xxxx')
<!-- 新增字段 -->
queryInterface.addColumn('表名','字段名',{})
<!-- 删除字段 -->
queryInterface.removeColumn('表名','字段名',{})
<!-- 字段相关配置参考（
https://sequelize.org/api/v6/class/src/model.js~model
搜索 attributes.column.type） -->
<!-- 例子 -->
'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return await queryInterface.createTable('User', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
      },
      user_name: {
        type: Sequelize.CHAR(10),
        comment: '名字',
        allowedNull: false
      },
      create_time: {
          type: Sequelize.BIGINT,
          comment: '创建时间',
          allowedNull: false
        },
        update_time: {
          type: Sequelize.BIGINT,
          comment: '更新时间',
          allowedNull: false
        },
        status: {
          type: Sequelize.INTEGER,
          comment: "状态0-未激活状态，1-激活状态",
          allowedNull: false,
          defaultValue: 1,
        },
    }, {
        createdAt: "create_time",
        updatedAt: "update_time",
        comment: "用户表",
      })
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return queryInterface.dropTable('users');
  }
};


<!-- Sequelize相关数据类型说明  -->
Sequelize 类型	数据库类型	备注
Sequelize.STRING  	VARCHAR(255)
Sequelize.STRING(1234) 	 VARCHAR(1234)
Sequelize.TEXT	TEXT
Sequelize.TEXT('tiny') 	TINYTEXT
Sequelize.INTEGER	INTEGER
Sequelize.BIGINT	BIGINT
Sequelize.BIGINT(11)	BIGINT(11)
Sequelize.FLOAT(11, 12) 	FLOAT(11,12)
Sequelize.REAL 	REAL      	 仅限于PostgreSQL.
Sequelize.REAL(11) 	REAL(11)	仅限于PostgreSQL
Sequelize.REAL(11, 12) 	REAL(11,12)	仅限于PostgreSQL
Sequelize.DOUBLE	DOUBLE
Sequelize.DOUBLE(11)	DOUBLE(11)
Sequelize.DOUBLE(11, 12)	DOUBLE(11,12)
Sequelize.DECIMAL 	DECIMAL
Sequelize.DECIMAL(10, 2)	DECIMAL(10,2)
Sequelize.DATE 	 DATETIME	针对 mysql / sqlite, TIMESTAMP WITH TIME ZONE 针对 postgres
Sequelize.DATE(6)	DATETIME(6)	 针对 mysql 5.6.4+. 小数秒支持多达6位精度
Sequelize.DATEONLY 	DATE	不带时间
Sequelize.BOOLEAN	TINYINT(1)
Sequelize.ENUM('value 1', 'value 2')		一个允许具有 “value 1” 和 “value 2” 的 ENUM
Sequelize.ARRAY(Sequelize.TEXT) 		定义一个数组。 仅限于 PostgreSQL。
Sequelize.ARRAY(Sequelize.ENUM)		定义一个 ENUM 数组. 仅限于 PostgreSQL
Sequelize.JSON 		JSON 列. 仅限于 PostgreSQL, SQLite and MySQL.
Sequelize.UUID		PostgreSQL 和 SQLite 的 UUID 数据类型, CHAR(36) BINARY 针对于 MySQL (使用默认值:Sequelize.UUIDV1 或 Sequelize.UUIDV4 来让 sequelize 自动生成 ID)
Sequelize.GEOMETRY		空间列.  仅限于 PostgreSQL (具有 PostGIS) 或 MySQL.
Sequelize.GEOMETRY('POINT') 		具有几何类型的空间列.  仅限于 PostgreSQL (具有 PostGIS) 或 MySQL.
Sequelize.GEOMETRY('POINT', 4326)		具有几何类型和SRID的空间列.  仅限于 PostgreSQL (具有 PostGIS) 或 MySQL
```

4. 执行迁移 js

```
npm run up
```

5. 回退上一步迁移操作

```
npm run down
```

6. 回退所有迁移（<span style="font-weight: bold;color:red;">除了本地数据库可使用外不推荐其他环境下使用以下命令</span>）

```
<!-- 慎用  -->
npx sequelize db:migrate:undo:all
```

7. 创建种子数据（表的默认数据或测试数据）

```
npm run add:mock [名字]
```

8. 执行创建种子数据

```
npm run up:mock:all
```

9. 回退创建种子数据

```
npm run down:mock:all
```

10. 根据数据库表生成对应的 model

```
npm run model
```

### 接口文档

接口采用 apidoc 生成（参考文档 https://zhuanlan.zhihu.com/p/83487114 ）

生成指令

```
npm run api
```
