import { EggLogger } from 'egg';
import { Inject, HTTPController, HTTPMethod, HTTPMethodEnum, Context, EggContext, HTTPBody } from '@eggjs/tegg';
import { HelloService } from '@/service/test';

@HTTPController({
  path: '/',
})
export class TestController {
  @Inject()
  helloService: HelloService;

  @Inject()
  logger: EggLogger;

  /**
   * 
   * @api {get} / 新即可偶
   * @apiName apiName
   * @apiGroup group
   * @apiVersion  1.0.0
   * 
   * 
   * @apiParam  {String} paramName description
   * 
   * @apiSuccess (200) {json} name description
   * 
   * @apiParamExample  {json} Request-Example:
   * {
   *     property : value
   * }
   * 
   * 
   * @apiSuccessExample {json} Success-Response:
   * {
   *     property : value
   * }
   * 
   * 
   */
  @HTTPMethod({
    method: HTTPMethodEnum.GET,
    path: '/',
  })
  async index(@Context() ctx: EggContext, @HTTPBody() paramName: String) {
    this.logger.info('hello egg logger');
    // console.log(this.helloService);
    this.helloService.hello('123');
    return {}
  }

  @HTTPMethod({
    method: HTTPMethodEnum.GET,
    path: '/test',
  })
  async test(@Context() ctx: EggContext) {
    // this.logger.info('hello egg logger');
    // console.log(this.helloService);
    this.helloService.hello('123');
    ctx.redirect('/public/dist/index.html')

    return { tet: 'ccccccccccc' };
  }
}
