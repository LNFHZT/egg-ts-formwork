// This file is created by egg-ts-helper@1.35.1
// Do not modify this file!!!!!!!!!
/* eslint-disable */

import 'egg';
import ExportArticle from '../../../app/model/article';
import ExportAssort from '../../../app/model/assort';
import ExportBook from '../../../app/model/book';
import ExportMiddleBookAssort from '../../../app/model/middleBookAssort';
import ExportSequelizemetum from '../../../app/model/sequelizemetum';

declare module 'egg' {
  interface IModel {
    Article: ReturnType<typeof ExportArticle>;
    Assort: ReturnType<typeof ExportAssort>;
    Book: ReturnType<typeof ExportBook>;
    MiddleBookAssort: ReturnType<typeof ExportMiddleBookAssort>;
    Sequelizemetum: ReturnType<typeof ExportSequelizemetum>;
  }
}
